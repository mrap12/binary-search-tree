#include <stdio.h>
#include <stdlib.h>



struct Baum
{
    int wert;
    struct baum *rechts;
    struct baum *links;

};typedef struct Baum BAUM;

int zahl;

BAUM *einordnen(BAUM *ptr)
{
    if(ptr==NULL)
    {
        ptr=malloc(sizeof(BAUM));
        if(ptr==NULL)
        {
            printf("Kein freier Speicher");
            exit(EXIT_FAILURE);
        }
        ptr->wert=zahl;
        ptr->links=NULL;
        ptr->rechts=NULL;
    }
    else if(ptr->wert<zahl)
    {
        ptr->rechts=einordnen(ptr->rechts);
    }
    else if(ptr->wert>=zahl)
    {
        ptr->links=einordnen(ptr->links);
    }
    return(ptr);
}

BAUM *suchen(BAUM *ptr,int z)
{
    if(ptr=NULL)
    {
        return NULL;
    }
    else if(ptr->wert<z)
    {
        return suchen(ptr->rechts,z);
    }
    else if(ptr->wert==z)
    {
        return(ptr);
    }
    else
    {
        return suchen(ptr->links,z);
    }

}

void inorder(BAUM *ptr)
{
    if(ptr!=NULL)
    {
        inorder(ptr->links);
        printf("%d,",ptr->wert);
        inorder(ptr->rechts);
    }
}

void preorder(BAUM *ptr)
{
    if(ptr!=NULL)
    {
        printf("%d,",ptr->wert);
        preorder(ptr->links);
        preorder(ptr->rechts);
    }
}

void postorder(BAUM *ptr)
{
    if(ptr!=NULL)
    {
        postorder(ptr->links);
        postorder(ptr->rechts);
        printf("%d,",ptr->wert);
    }
}

int main()
{
    BAUM *root=NULL;
    int i=0;


    printf("Anzahl der Werte eingeben:");
    int anzahl;


    scanf("%d",&anzahl);

    for(i=0;i<anzahl;i++)
    {
        printf("Zahl eingeben:");
        scanf("%d",&zahl);
        root=einordnen(root);

    }
    printf("preorder:");
    preorder(root);
    printf("\n");
    printf("inorder:");
    inorder(root);
    printf("\n");
    printf("postorder:");
    postorder(root);

    return 0;
}
